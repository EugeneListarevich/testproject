﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac.Integration.WebApi;
using Autofac.Configuration;
using Microsoft.Extensions.Configuration;
using Autofac;
using Autofac.Core;
using System.Web.Http;
using System.Reflection;

using Autofac.Integration.Mvc;



namespace Eugeen.WebAPI.Util
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            var config = new ConfigurationBuilder();
            config.AddJsonFile("autofac.json");
            var module = new ConfigurationModule(config.Build());
            var builder = new ContainerBuilder();
            builder.RegisterModule(module);
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<DAL.Mongo.UnitOfWork>().As<DAL.Interfaces.IUOWMongo>();
            builder.RegisterType<DAL.SQL.UnitOfWork>().As<DAL.Interfaces.IUOWSQL>();
            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }
    }
}