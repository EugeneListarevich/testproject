﻿using Eugeen.DAL.Interfaces;
using Eugeen.DAL.Interfaces.DTO;
using Eugeen.DAL.SQL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Eugeen.WebAPI.Controllers
{
    public class SQLController : ApiController
    {
        private IUOWSQL db;
        public SQLController(IUOWSQL UOW)
        {
            db = UOW;
        }
        [HttpGet]
        [Route("sql/name")]
        public IHttpActionResult SearchByName(string name)
        {
            IEnumerable<IProduct> prods = db.ProductDAO.GetProductsByName(name);
            return Ok(prods);
        }
        [HttpGet]
        [Route("sql/brand")]
        public IHttpActionResult SearchByBrand(string brand)
        {
            IEnumerable<IProduct> prods = db.ProductDAO.GetProductsByManufacturer(brand);
            return Ok(prods);
        }
        [HttpGet]
        [Route("sql/id")]
        public IHttpActionResult SearchById(string id)
        {
            IProduct prods = db.ProductDAO.GetProductById(id);
            return Ok(prods);
        }
    }
}
