﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Eugeen.DAL.Interfaces.DAO;
using Eugeen.DAL.Interfaces.DTO;
using Eugeen.DAL.Mongo.DTO;
using Eugeen.DAL.Mongo.Entities;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Eugeen.DAL.Mongo.DAO
{
    class ProductDAO : IProductDAO
    {
        private IMongoDatabase db { get; set; }
        private const string CollectionName = "Products";
        private IMongoCollection<ProductMongo> Collection { get; set; }
        public ProductDAO()
        {
            db = new MongoClient("mongodb://192.168.25.77:27017/spot_staging").GetDatabase("spot_staging");
            Collection = db.GetCollection<ProductMongo>(CollectionName);
            Util.AutoMapper.Initialize();
        }
        
        
        public IProduct GetProductById(string productId)
        {
            var filter = new BsonDocument("_id", new ObjectId(productId));
            var productMongo = Collection.Find(filter).FirstOrDefault();
            IProduct product = Mapper.Map<ProductMongo, Product>(productMongo);
            return product;
        }

        public IEnumerable<IProduct> GetProductsByManufacturer(string manufacturer)
        {
            var filter = new BsonDocument("manufacturer", BsonRegularExpression.Create(new Regex(manufacturer,RegexOptions.IgnoreCase)));
            var productsMongo = Collection.Find(filter).ToList();
            IEnumerable<IProduct> products = 
                Mapper.Map<IEnumerable<ProductMongo>, List<Product>>(productsMongo);
            return products;
        }

        public IEnumerable<IProduct> GetProductsByName(string name)
        {
            var filter = new BsonDocument("details", new BsonDocument("$elemMatch", new BsonDocument("name", BsonRegularExpression.Create(new Regex(name, RegexOptions.IgnoreCase)))));
            var productsMongo = Collection.Find(filter).ToList();
            IEnumerable<IProduct> products =
                Mapper.Map<IEnumerable<ProductMongo>, List<Product>>(productsMongo);
            return products;
        }
    }
}
