﻿using MongoDB.Bson.Serialization.Attributes;

namespace Eugeen.DAL.Mongo.Entities
{
    public class ProductPhotoMongo
    {
        [BsonElement("full")]
        public string Full { get; set; }
        [BsonElement("thumbnail")]
        public string Thumbnail { get; set; }
        [BsonElement("order")]
        public int Order { get; set; }
    }
}