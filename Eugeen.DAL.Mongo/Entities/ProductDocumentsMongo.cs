﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Eugeen.DAL.Mongo.Entities
{
    public class ProductDocumentsMongo
    {
        [BsonElement("name")]
        public string DocumentName { get; set; }
        [BsonElement("type")]
        public string TypeName { get; set; }
        [BsonElement("file_service_id")]
        public string FileServiceId { get; set; }
        [BsonElement("сertifcate_id")]
        public string CertificateId { get; set; }
        [BsonElement("file_extension")]
        public string FileExtension { get; set; }
        [BsonElement("mark_id")]
        public string MarkImageId { get; set; }
        [BsonElement("url")]
        public string Url { get; set; }
        [BsonElement("end_time")]
        public DateTime? EndTime { get; set; }
        [BsonElement("params")]
        public List<ProductDocumentParam> Params { get; set; }
    }
}