﻿using MongoDB.Bson.Serialization.Attributes;

namespace Eugeen.DAL.Mongo.Entities
{
    public class FacetMongo
    {
        [BsonElement("facet_name")]
        public string FacetName { get; set; }
        [BsonElement("facet_lang")]
        public string FacetLang { get; set; }
        [BsonElement("facet_value")]
        public string FacetValue { get; set; }
        [BsonElement("facet_source")]
        public string FacetSource { get; set; }
        [BsonElement("facet_level")]
        public int FacetLevel { get; set; }
    }
}