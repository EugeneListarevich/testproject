﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Eugeen.DAL.Mongo.Entities
{
    public class ProductMultilangDataMongo
    {
        [BsonElement("language")]
        public string Language { get; set; }
        [BsonElement("name")]
        public string Name { get; set; }
        [BsonElement("description")]
        public string Description { get; set; }
        [BsonElement("short_description")]
        public string ShortDescription { get; set; }
        [BsonElement("keywords")]
        public IEnumerable<string> Keywords { get; set; }
        [BsonElement("retail_locations")]
        public IEnumerable<string> RetailLocations { get; set; }
        [BsonElement("regions")]
        public IEnumerable<string> Regions { get; set; }
    }
}