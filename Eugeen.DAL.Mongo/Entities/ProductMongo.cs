﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eugeen.DAL.Mongo.Entities
{
    class ProductMongo
    {
        [BsonElement("_id")]
        public ObjectId Id { get; set; }
        [BsonElement("source_id")]
        public string UleProductId { get; set; }
        [BsonElement("source_system")]
        public string SourceSystem { get; set; }
        [BsonElement("photos")]
        public List<ProductPhotoMongo> Photos { get; set; }
        [BsonElement("documents")]
        public List<ProductDocumentsMongo> Documents { get; set; }
        [BsonElement("launch_date")]
        public DateTime? LaunchDate { get; set; }
        [BsonElement("listing_date")]
        public DateTime? ListingDate { get; set; }
        [BsonElement("modified_date")]
        public DateTime ModifiedDate { get; set; }
        [BsonElement("created_date")]
        public DateTime CreatedDate { get; set; }
        [BsonElement("is_active")]
        public bool IsActive { get; set; }
        [BsonElement("manufacturer")]
        public string Manufacturer { get; set; }
        [BsonElement("string_facets")]
        public List<FacetMongo> Facets { get; set; }
        [BsonElement("details")]
        public List<ProductMultilangDataMongo> Details { get; set; }
    }
}
