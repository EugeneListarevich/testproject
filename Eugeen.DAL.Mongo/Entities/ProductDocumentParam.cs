﻿using MongoDB.Bson.Serialization.Attributes;

namespace Eugeen.DAL.Mongo.Entities
{
    public class ProductDocumentParam
    {
        [BsonElement("name")]
        public string Name { get; set; }
        [BsonElement("value")]
        public int Value { get; set; }
    }
}