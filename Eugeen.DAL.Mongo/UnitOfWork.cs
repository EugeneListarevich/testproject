﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eugeen.DAL.Interfaces;
using Eugeen.DAL.Interfaces.DAO;
using Eugeen.DAL.Mongo.DAO;

namespace Eugeen.DAL.Mongo
{
    public class UnitOfWork : IUOWMongo
    {
        public UnitOfWork()
        {
            ProductDAO = new ProductDAO();
        }
        public IProductDAO ProductDAO { get; private set; }
    }
}
