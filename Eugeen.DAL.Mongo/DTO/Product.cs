﻿using Eugeen.DAL.Interfaces.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eugeen.DAL.Mongo.DTO
{
    class Product : IProduct
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
    }
}
