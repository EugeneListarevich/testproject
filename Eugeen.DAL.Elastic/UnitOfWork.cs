﻿using Eugeen.DAL.Elastic.DAO;
using Eugeen.DAL.Interfaces;
using Eugeen.DAL.Interfaces.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eugeen.DAL.Elastic
{
    public class UnitOfWork : IUOWElastic
    {
        public UnitOfWork()
        {
            ProductDAO = new ProductDAO();
        }
        public IProductDAO ProductDAO { get; private set; }
    }
}
