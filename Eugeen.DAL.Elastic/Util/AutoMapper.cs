﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Eugeen.DAL.Elastic.DTO;
using Eugeen.DAL.Elastic.Entities;

namespace Eugeen.DAL.Elastic.Util
{
    public static class AutoMapper
    {
        private static bool IsInit = false;
        public static void Initialize()
        {
            if (!IsInit)
            {
                try
                {
                    Mapper.Initialize(cfg => cfg.CreateMap<ProductElastic, Product>()
                                                    .ForMember("Name", x => x.MapFrom(f => f.Details.FirstOrDefault().Name))
                                                    .ForMember("Brand", x => x.MapFrom(f => f.Manufacturer))
                                                    .ForMember("Id", x => x.MapFrom(f => f.Id)));
                    
                }
                catch
                {
                    Mapper.Reset();
                    Mapper.Initialize(cfg => cfg.CreateMap<ProductElastic, Product>()
                                                    .ForMember("Name", x => x.MapFrom(f => f.Details.FirstOrDefault().Name))
                                                    .ForMember("Brand", x => x.MapFrom(f => f.Manufacturer))
                                                    .ForMember("Id", x => x.MapFrom(f => f.Id)));
                }
                finally
                {
                    IsInit = true;
                }
            }

            //Mapper.Initialize(cfg => cfg.CreateMap<List<ProductMongo>, IEnumerable<Product>>()
            //    .ForMember("Name", x => x.MapFrom(f => f.Details.FirstOrDefault().Name))
            //    .ForMember("Brand", x => x.MapFrom(f => f.Manufacturer))
            //    .ForMember("Id", x => x.MapFrom(f => f.Id)));
        }
    }
}
