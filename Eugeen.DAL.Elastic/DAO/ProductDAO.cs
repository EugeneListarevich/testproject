﻿using Eugeen.DAL.Elastic.Entities;
using Eugeen.DAL.Interfaces.DAO;
using Eugeen.DAL.Interfaces.DTO;
using Nest;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Eugeen.DAL.Elastic.DTO;

namespace Eugeen.DAL.Elastic.DAO
{
    public class ProductDAO : IProductDAO
    {
        ConnectionSettings settings;
        ElasticClient client;
        public ProductDAO()
        {
            settings = new ConnectionSettings(new Uri("http://192.168.25.77:9200")).DefaultIndex("products").DisableDirectStreaming();
            client = new ElasticClient(settings);
            Util.AutoMapper.Initialize();
        }
        
        public IProduct GetProductById(string productId)
        {
            ProductElastic res
                = client.Search<ProductElastic>(s => s
                    .Size(100)
                    .Query(q => q
                        .Bool(b => b
                            .Must(mu => mu
                                .Regexp(c => c.Field(p => p.Id).Value(productId)
                                        ))))).Documents.FirstOrDefault();
            IProduct product = Mapper.Map<ProductElastic, Product>(res);
            return product;
        }

        public IEnumerable<IProduct> GetProductsByManufacturer(string manufacturer)
        {
            IEnumerable<ProductElastic> res
                = client.Search<ProductElastic>(s => s
                    .Size(100)
                    .Query(q => q
                        .Bool(b => b
                            .Must(mu => mu
                                .Regexp(c => c.Field(p => p.Manufacturer).Value(".*" + manufacturer + ".*")
                                        ))))).Documents;

            IEnumerable<IProduct> products = Mapper.Map<IEnumerable<ProductElastic>, List<Product>>(res);

            return products;
        }

        public IEnumerable<IProduct> GetProductsByName(string name)
        {
            IEnumerable<ProductElastic> res
               = client.Search<ProductElastic>(s => s
                   .Size(100)
                   .Query(q => q
                       .Nested(c1 => c1
                           .Path(p => p.Details)
                           .Query(q2 => q2
                               .Bool(b => b
                                    .Must(mu => mu
                                        .Regexp(c => c
                                            .Field(p => p.Details.First().Name)
                                            .Value(".*" + name + ".*")
                                       ))))))).Documents;
            IEnumerable<IProduct> products = Mapper.Map<IEnumerable<ProductElastic>, List<Product>>(res);
            return products;
        }
    }
}
