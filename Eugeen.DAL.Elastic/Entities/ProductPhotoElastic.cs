﻿using Nest;

namespace Eugeen.DAL.Elastic.Entities
{
    public class ProductPhotoElastic
    {
        [Keyword(Name = "full")]
        public string Full { get; set; }
        [Keyword(Name = "thumbnail")]
        public string Thumbnail { get; set; }
        [Number(Name = "order")]
        public int Order { get; set; }
    }
}
