﻿using Nest;

using System;
using System.Collections.Generic;

namespace Eugeen.DAL.Elastic.Entities
{
    [ElasticsearchType(Name = "product")]
    public class ProductElastic
    {
        public const string IdAttr = "id";
        public const string FacetsAttr = "string_facets";
        public const string FullTextAttr = "full_text";
        public const string DetailsAttr = "details";
        public const string ManufacturerAttr = "manufacturer";
        public const string IsActiveAttr = "is_active";
        public const string LaunchDateAttr = "launch_date";
        public const string ListingDateAttr = "listing_date";

        [Keyword(Name = IdAttr)]
        public string Id { get; set; }
        [Nested(Name = "photos")]
        public IEnumerable<ProductPhotoElastic> Photos { get; set; }
        [Nested(Name = "documents")]
        public IEnumerable<ProductDocumentElastic> Documents { get; set; }
        [Date(Name = LaunchDateAttr)]
        public DateTime? LaunchDate { get; set; }
        [Date(Name = ListingDateAttr)]
        public DateTime? ListingDate { get; set; }
        [Date(Name = "modified_date")]
        public DateTime ModifiedDate { get; set; }
        [Date(Name = "created_date")]
        public DateTime CreatedDate { get; set; }
        [Boolean(Name = IsActiveAttr)]
        public bool IsActive { get; set; }
        [Keyword(Name = ManufacturerAttr)]
        public string Manufacturer { get; set; }
        [Nested(Name = FacetsAttr)]
        public IEnumerable<FacetElastic> Facets { get; set; }
        [Nested(Name = DetailsAttr)]
        public IEnumerable<ProductMultilangDataElastic> Details { get; set; }
        [Text(Name = FullTextAttr)]
        public string FullText { get; set; }
    }
}
