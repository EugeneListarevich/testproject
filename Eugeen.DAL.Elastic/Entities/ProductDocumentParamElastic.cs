﻿using Nest;

namespace Eugeen.DAL.Elastic.Entities
{
    public class ProductDocumentParamElastic
    {
        [Keyword(Name = "name")]
        public string Name { get; set; }
        [Keyword(Name = "value")]
        public int Value { get; set; }
    }
}
