﻿using System.Collections.Generic;

namespace Eugeen.DAL.Elastic.Entities
{
    public class ProductDetailsElastic
    {
        public string ProductId { get; set; }
        public string Language { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public IEnumerable<string> Keywords { get; set; }
    }
}
