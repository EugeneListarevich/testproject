﻿using Nest;

namespace Eugeen.DAL.Elastic.Entities
{
    public class FacetElastic
    {
        public const string FacetNameAttr = "facet_name";
        public const string FacetValueAttr = "facet_value";
        public const string FacetLangAttr = "facet_lang";
        public const string FacetLevelAttr = "facet_level";
        public const string FacetSourceAttr = "facet_source";

        [Keyword(Name = FacetNameAttr)]
        public string FacetName { get; set; }
        [Keyword(Name = FacetLangAttr)]
        public string FacetLang { get; set; }
        [Keyword(Name = FacetValueAttr)]
        public string FacetValue { get; set; }
        [Keyword(Name = FacetSourceAttr)]
        public string FacetSource { get; set; }
        [Number(Name = FacetLevelAttr)]
        public int FacetLevel { get; set; }
    }
}
