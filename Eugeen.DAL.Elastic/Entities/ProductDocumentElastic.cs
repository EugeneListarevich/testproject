﻿using Nest;
using System;
using System.Collections.Generic;

namespace Eugeen.DAL.Elastic.Entities
{
    public class ProductDocumentElastic
    {
        [Keyword(Name = "name")]
        public string DocumentName { get; set; }
        [Keyword(Name = "type")]
        public string TypeName { get; set; }
        [Keyword(Name = "file_service_id")]
        public string FileServiceId { get; set; }
        [Keyword(Name = "сertifcate_id")]
        public string CertificateId { get; set; }
        [Keyword(Name = "file_extension")]
        public string FileExtension { get; set; }
        [Keyword(Name = "mark_id")]
        public string MarkImageId { get; set; }
        [Keyword(Name = "url")]
        public string Url { get; set; }
        [Date(Name = "end_time")]
        public DateTime? EndTime { get; set; }
        [Nested(Name = "params")]
        public List<ProductDocumentParamElastic> Params { get; set; }
    }
}
