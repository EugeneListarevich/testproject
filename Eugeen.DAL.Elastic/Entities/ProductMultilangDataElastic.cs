﻿using Nest;
using System.Collections.Generic;

namespace Eugeen.DAL.Elastic.Entities
{
    public class ProductMultilangDataElastic
    {
        public const string LanguageAttr = "language";
        public const string NameAttr = "name";

        [Keyword(Name = LanguageAttr)]
        public string Language { get; set; }
        [Keyword(Name = NameAttr)]
        public string Name { get; set; }
        [Text(Name = "description")]
        public string Description { get; set; }
        [Text(Name = "short_description")]
        public string ShortDescription { get; set; }
        [Text(Name = "keywords")]
        public IEnumerable<string> Keywords { get; set; }
        [Text(Name = "retail_locations")]
        public IEnumerable<string> RetailLocations { get; set; }
        [Text(Name = "regions")]
        public IEnumerable<string> Regions { get; set; }
    }
}
