﻿using Eugeen.DAL.Interfaces.DAO;
using Eugeen.DAL.Interfaces.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using Eugeen.DAL.SQL.DTO;


namespace Eugeen.DAL.SQL.DAO
{
    class ProductDAO : IProductDAO
    {
        private const string connStr = "Data Source=192.168.25.77\\SQLSERVER2016;Database=GreenGuard;User Id=cp_gr33nguard;Password=gr33nguardt0x1c;";
        public IProduct GetProductById(string productId)
        {
            using(SqlConnection connection = new SqlConnection(connStr))
            {
                IProduct product = connection.Query<Product>(
                    Queries.GetProductByIdQuery,
                    new { productId }).FirstOrDefault();
                return product;
            }
        }

        public IEnumerable<IProduct> GetProductsByManufacturer(string manufacturer)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                IEnumerable<IProduct> products = connection.Query<Product>(
                    Queries.GetProductsByNameQuery,
                    new { manufacturer });
                return products;
            }
        }

        public IEnumerable<IProduct> GetProductsByName(string name)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                IEnumerable<IProduct> products = connection.Query<Product>(
                    Queries.GetProductsByNameQuery,
                    new { name });
                return products;
            }
        }

    }
}
