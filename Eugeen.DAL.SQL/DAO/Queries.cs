﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eugeen.DAL.SQL.DAO
{
    public static class Queries
    {
        public const string GetProductByIdQuery = @"
                SELECT
                  prd.ProductId                AS Id,
                  prd.ProductName              AS Name,
                  mnf.Companyname              AS Brand
                FROM dbo.PlusProduct prd
                  LEFT JOIN dbo.Manufacturers mnf ON mnf.ManufacturerId = prd.ManufacturerID
                WHERE (ProductID = @productId OR @productId IS NULL)
            ";
        public const string GetProductsByNameQuery = @"
                SELECT
                    prd.ProductId               AS Id,
                    prd.ProductName             AS Name,
                    mnf.Companyname             AS Brand
                FROM dbo.PlusProduct prd
                    LEFT JOIN dbo.Manufacturers mnf ON mnf.ManufacturerId = prd.ManufacturerID
                WHERE (ProductName LIKE '%'+@name+'%')
            ";
        public const string GetProductsByBrandQuery = @"
                SELECT
                    prd.ProductId               AS Id,
                    prd.ProductName             AS Name,
                    mnf.Companyname             AS Brand
                FROM dbo.Manufacturers mnf 
                    LEFT JOIN dbo.PlusProduct prd ON prd.ManufacturerID = mnf.ManufacturerId 
                WHERE (mnf.Companyname LIKE '%'+@manufacturer+'%')
            ";

    }
}
