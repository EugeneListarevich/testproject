﻿using Eugeen.DAL.Interfaces;
using Eugeen.DAL.Interfaces.DAO;
using Eugeen.DAL.SQL.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eugeen.DAL.SQL
{
    public class UnitOfWork:IUOWSQL
    {
        public UnitOfWork()
        {
            ProductDAO = new ProductDAO();
                
        }
        public IProductDAO ProductDAO { get; private set; }
    }
}
