﻿using Eugeen.DAL.Interfaces.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eugeen.DAL.Interfaces
{
    public interface IUOWElastic
    {
        IProductDAO ProductDAO { get; }
    }
}
