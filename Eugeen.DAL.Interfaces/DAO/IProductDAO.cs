﻿using Eugeen.DAL.Interfaces.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eugeen.DAL.Interfaces.DAO
{
    public interface IProductDAO
    {
        IEnumerable<IProduct> GetProductsByName(string name);
        IEnumerable<IProduct> GetProductsByManufacturer(string manufacturer);
        IProduct GetProductById(string productId);
    }
}
