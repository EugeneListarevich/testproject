﻿using Eugeen.DAL.Interfaces.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eugeen.DAL.Interfaces
{
    public interface IUOWMongo
    {
        IProductDAO ProductDAO { get; }
    }
}
